package ru.wk.telegram.foodbot.command;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

import static java.lang.System.lineSeparator;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.joining;
import static ru.wk.telegram.foodbot.command.Commands.values;

public class HelpCommand implements Command {

    @Override
    public SendMessage execute(Message request) {
        String commands = stream(values())
                .map(command -> command.getName() + " - " + command.getDescription())
                .collect(joining(lineSeparator()));
        return new SendMessage(request.getChatId(), commands);
        // TODO: 16.03.2018 admin filter
    }
}
