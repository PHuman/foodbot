package ru.wk.telegram.foodbot.command;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;

import static ru.wk.telegram.foodbot.bot.OrderHandler.cleanOrder;

public class CancelCommand implements Command {
    @Override
    public SendMessage execute(Message request) {
        cleanOrder(request);
        return new SendMessage()
                .setChatId(request.getChatId())
                .setText("Твой заказ отменен" + '\uD83D' + '\uDE04')
                .setReplyMarkup(new ReplyKeyboardRemove());
    }
}
