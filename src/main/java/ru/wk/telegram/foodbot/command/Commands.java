package ru.wk.telegram.foodbot.command;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

import static java.util.Arrays.stream;

public enum Commands {
    HELP("/help", "Список доступных команд", new HelpCommand()),
    MENU("/menu", "Меню на сегодня", new MenuCommand()),
    CANCEL("/cancel", "Отменить сегодняшний заказ", new CancelCommand()),
    SEND("/send", "Отправить всем пользователям меню за сегодня", new SendCommand()),
    ORDER("/order", "Просмотреть сегодняшний заказ, /order all - заказы всех, /order list - общий заказ", new OrderCommand()),
    USERS("/users", "Получить список всех пользователей", new UsersCommand()),
    BALANCE("/balance", "Узнать личный баланс либо баланс бользователя: /balance userid", new BalanceCommand()),
    ADD("/add", "Добавить деньги на счет пользователю: /add userid amount", new AddCommand());

    private final String name;
    private final String description;
    private final Command command;

    Commands(String name, String description, Command command) {
        this.name = name;
        this.description = description;
        this.command = command;
    }

    public static SendMessage getMessage(Message request) {
        return stream(Commands.values())
                .filter(command -> request.getText().startsWith(command.name))
                .map(command -> command.command.execute(request))
                .findFirst()
                .orElse(null);
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Command getCommand() {
        return command;
    }
}
