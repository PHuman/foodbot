package ru.wk.telegram.foodbot.command;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.List;

import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static ru.wk.telegram.foodbot.bot.BotConstants.CANCEL_ORDER;
import static ru.wk.telegram.foodbot.bot.BotConstants.MAKE_ORDER;
import static ru.wk.telegram.foodbot.site.Parser.parseFoodSite;

public class MenuCommand implements Command {

    @Override
    public SendMessage execute(Message request) {
        List<ImmutablePair<String, String>> foodFromSite = parseFoodSite();
        String food = foodFromSite.stream()
                .map(pair -> pair.left + " - " + pair.right)
                .collect(joining(lineSeparator()));

        return new SendMessage(request.getChatId(), food).setReplyMarkup(getKeyBoard(foodFromSite));
    }

    private ReplyKeyboardMarkup getKeyBoard(List<ImmutablePair<String, String>> foodFromSite) {
        ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup()
                .setSelective(true)
                .setResizeKeyboard(true);

        List<KeyboardRow> rows = foodFromSite.stream()
                .map(this::createRowFromFood)
                .collect(toList());

        KeyboardRow row = new KeyboardRow();
        row.add(CANCEL_ORDER);
        row.add(MAKE_ORDER);
        rows.add(row);

        keyboard.setKeyboard(rows);
        return keyboard;
    }

    private KeyboardRow createRowFromFood(ImmutablePair<String, String> food) {
        KeyboardRow row = new KeyboardRow();
        row.add(food.left);
        return row;
    }
}
