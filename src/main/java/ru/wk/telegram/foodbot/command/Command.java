package ru.wk.telegram.foodbot.command;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

import static ru.wk.telegram.foodbot.bot.BotConstants.COMMAND_UNAVAILABLE;

public interface Command {

    default SendMessage execute(Message request) {
        return new SendMessage()
                .setChatId(request.getChatId())
                .setText(COMMAND_UNAVAILABLE);
    }
}
