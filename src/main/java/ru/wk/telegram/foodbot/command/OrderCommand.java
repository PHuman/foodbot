package ru.wk.telegram.foodbot.command;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
import ru.wk.telegram.foodbot.bot.OrderHandler;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.ImmutableSet.of;
import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.*;
import static ru.wk.telegram.foodbot.bot.BotConstants.EMPTY_ORDER;
import static ru.wk.telegram.foodbot.bot.OrderHandler.*;

public class OrderCommand implements Command {
    @Override
    public SendMessage execute(Message request) {
        SendMessage response = new SendMessage()
                .setChatId(request.getChatId())
                .setReplyMarkup(new ReplyKeyboardRemove())
                .setText(EMPTY_ORDER);
        if (request.getText().contains("all")) {
            Set<String> userNames = OrderHandler.orders.keySet();
            generateOrderMessage(userNames, response);
        } else if (request.getText().contains("list")) {
            generateOrderList(response);
        } else if (!isEmptyOrder(request.getFrom().getUserName())) {
            generateOrderMessage(of(request.getFrom().getUserName()), response);
        }
        return response;
    }

    private void generateOrderMessage(Set<String> userNames, SendMessage response) {
        String msg = userNames.stream()
                .filter(userName -> !isEmptyOrder(userName))
                .map(OrderHandler::generateMessage)
                .collect(joining(lineSeparator()));
        response.setText(msg);
    }

    private void generateOrderList(SendMessage response) {
        Collection<ImmutablePair<String, String>> values = orders.values();
        Map<String, Long> foodByCount = values.stream().collect(groupingBy(food -> food.left, counting()));
        String msg = foodByCount.entrySet().stream()
                .map(entity -> entity.getKey() + " - " + entity.getValue())
                .collect(joining(lineSeparator()));
        Double sum = calculateSum(values);
        response.setText(msg + lineSeparator() + (sum == 0 ? "Заказ пуст" : "на сумму: " + format("%(.2f руб.", sum)));
    }
}
