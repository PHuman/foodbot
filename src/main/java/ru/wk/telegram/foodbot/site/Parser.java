package ru.wk.telegram.foodbot.site;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.log4j.Logger;
import ru.wk.telegram.foodbot.bot.FoodBot;

import java.io.IOException;
import java.time.DayOfWeek;
import java.util.List;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.time.LocalDate.now;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.apache.log4j.Logger.getLogger;
import static ru.wk.telegram.foodbot.bot.BotConstants.QUERY;
import static ru.wk.telegram.foodbot.bot.BotConstants.URI;

public class Parser {
    private static final Logger LOG = getLogger(FoodBot.class);

    private static List<ImmutablePair<String, String>> food = emptyList();
    private static volatile DayOfWeek currentDay;

    public static List<ImmutablePair<String, String>> parseFoodSite() {
        DayOfWeek today = now().getDayOfWeek();
//        DayOfWeek today = DayOfWeek.WEDNESDAY;
        String nameByDay = Days.getNameByDay(today);
        if (nameByDay == null) {
            return emptyList();
        }
        if (today.equals(currentDay) && !food.isEmpty()) {
            return food;
        }
        currentDay = today;

        return getFoodFromSite(nameByDay);
    }

    private static List<ImmutablePair<String, String>> getFoodFromSite(String nameByDay) {
        try (final WebClient webClient = new WebClient()) {
            final HtmlPage page = webClient.getPage(format(URI, nameByDay));
            List<?> byXPath = page.getByXPath(QUERY);

            return food = byXPath.stream()
                    .filter(HtmlDivision.class::isInstance)
                    .map(HtmlDivision.class::cast)
                    .map(DomNode::asText)
                    .map(el -> el.split(lineSeparator()))
                    .map(el -> new ImmutablePair<>(el[0].trim(), el[2].trim()))
                    .collect(toList());
        } catch (IOException e) {
            LOG.error(e);
            return emptyList();
        }
    }
}
