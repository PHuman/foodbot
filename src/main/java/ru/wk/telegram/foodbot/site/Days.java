package ru.wk.telegram.foodbot.site;

import java.time.DayOfWeek;

import static java.util.Arrays.stream;

public enum Days {
    MONDAY("ponedelnik", DayOfWeek.MONDAY),
    TUESDAY("vtornik", DayOfWeek.TUESDAY),
    WEDNESDAY("sreda", DayOfWeek.WEDNESDAY),
    THURSDAY("chetverg", DayOfWeek.THURSDAY),
    FRIDAY("pyatnica", DayOfWeek.FRIDAY);

    private String name;
    private DayOfWeek dayOfWeek;

    Days(String name, DayOfWeek dayOfWeek) {
        this.name = name;
        this.dayOfWeek = dayOfWeek;
    }

    public static String getNameByDay(DayOfWeek dayOfWeek) {
        return stream(Days.values())
                .filter(day -> day.dayOfWeek.equals(dayOfWeek))
                .map(days -> days.name)
                .findAny()
                .orElse(null);

    }
}
