package ru.wk.telegram.foodbot;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import ru.wk.telegram.foodbot.bot.FoodBot;

public class Main {
    public static void main(String[] args) {
        initBot();
    }


    private static void initBot() {
        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            botsApi.registerBot(new FoodBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
