package ru.wk.telegram.foodbot.bot;

public class BotConstants {
    public static final String TOKEN = "585273752:AAHPwDl1dPoJn_9dGQcgLbzR__qgRyX_l_I";
    public static final String BOT_USERNAME = "wk_food_bot";

    public static final String CANCEL_ORDER = "Очистить заказ";
    public static final String MAKE_ORDER = "Заказать";
    public static final String COMMAND_UNAVAILABLE = "Команда недоступна";
    public static final String EMPTY_ORDER = "Заказ пуст, чтобы сделать заказ посмотри /menu";

    public static final String URI = "http://dostavka.irina-service.by/%s";
    public static final String QUERY = "//div[contains(@class, 'caption mh-category')]";

    private BotConstants() {
    }
}
