package ru.wk.telegram.foodbot.bot;

import com.google.common.collect.ListMultimap;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static com.google.common.collect.LinkedListMultimap.create;
import static com.google.common.collect.Multimaps.synchronizedListMultimap;
import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.util.stream.Collectors.joining;
import static ru.wk.telegram.foodbot.bot.BotConstants.CANCEL_ORDER;
import static ru.wk.telegram.foodbot.bot.BotConstants.MAKE_ORDER;
import static ru.wk.telegram.foodbot.bot.FoodBot.getDefaultMessage;
import static ru.wk.telegram.foodbot.site.Parser.parseFoodSite;

public class OrderHandler {
    private static ListMultimap<String, ImmutablePair<String, String>> preOrders = synchronizedListMultimap(create());
    public static ListMultimap<String, ImmutablePair<String, String>> orders = synchronizedListMultimap(create());

    public static void cleanOrder(Message request) {
        preOrders.removeAll(request.getFrom().getUserName());
        orders.removeAll(request.getFrom().getUserName());
    }

    static SendMessage makeOrder(Message request) {
        List<ImmutablePair<String, String>> foods = parseFoodSite();
        String text = request.getText();
        String userName = request.getFrom().getUserName();

        ImmutablePair<String, String> pair = foods.stream()
                .filter(food -> food.left.equals(text))
                .findFirst()
                .orElse(null);

        boolean executable = execute(request);
        if (pair == null && !executable) {
            return getDefaultMessage(request);
        }
        changePreOrder(userName, pair);
        SendMessage sendMessage = new SendMessage(request.getChatId(), generateMessage(userName));
        if (executable && !isEmptyOrder(userName)) {
            sendMessage.setReplyMarkup(new ReplyKeyboardRemove());
        }
        return sendMessage;
    }

    public static String generateMessage(String userName) {
        List<ImmutablePair<String, String>> orderForCurrentUser = preOrders.get(userName);
        String order = orderForCurrentUser.stream()
                .filter(Objects::nonNull)
                .map(food -> food.left)
                .collect(joining(lineSeparator()));

        Double amount = calculateSum(orderForCurrentUser);

        String finished = isEmptyOrder(userName) ? "" : lineSeparator() + "завершен";
        return "Заказ для " + userName + lineSeparator() + order +
                (amount == 0 ? "" : lineSeparator() + "на сумму: " + format("%(.2f руб.", amount)) + finished;
    }

    public static Double calculateSum(Collection<ImmutablePair<String, String>> pairList) {
        return pairList.stream()
                .filter(Objects::nonNull)
                .map(food -> food.right.substring(0, food.right.indexOf(" ")))
                .mapToDouble(Double::valueOf)
                .sum();
    }

    public static boolean isEmptyOrder(String userName) {
        return orders.get(userName).stream().noneMatch(Objects::nonNull);
    }

    private static boolean execute(Message request) {
        if (MAKE_ORDER.equals(request.getText())) {
            String userName = request.getFrom().getUserName();
            List<ImmutablePair<String, String>> userOrder = preOrders.get(userName);
            orders.putAll(userName, userOrder);
            return true;
        }
        if (CANCEL_ORDER.equals(request.getText())) {
            cleanOrder(request);
            return true;
        }
        return false;
    }

    private static void changePreOrder(String userName, ImmutablePair<String, String> pair) {
        preOrders.put(userName, pair);
    }
}
