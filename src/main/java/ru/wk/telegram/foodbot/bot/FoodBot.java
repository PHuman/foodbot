package ru.wk.telegram.foodbot.bot;

import org.apache.log4j.Logger;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import static org.apache.log4j.Logger.getLogger;
import static ru.wk.telegram.foodbot.bot.BotConstants.BOT_USERNAME;
import static ru.wk.telegram.foodbot.bot.BotConstants.TOKEN;
import static ru.wk.telegram.foodbot.bot.OrderHandler.makeOrder;
import static ru.wk.telegram.foodbot.command.Commands.getMessage;

public class FoodBot extends TelegramLongPollingBot {
    private static final Logger LOG = getLogger(FoodBot.class);

    static SendMessage getDefaultMessage(Message request) {
        return new SendMessage()
                .setChatId(request.getChatId())
                .setText("попробуй еще" + '\uD83D' + '\uDE04')
                .setReplyMarkup(new ReplyKeyboardRemove());
    }

    public void onUpdateReceived(Update update) {
        Message request = update.getMessage();
        if (update.hasMessage() && request.hasText()) {
            LOG.info(request.getFrom() + " : " + request.getText());
            SendMessage message = getMessage(request);
            if (message == null) {
                message = makeOrder(request);
            }

            try {
                execute(message);
            } catch (TelegramApiException e) {
                LOG.error(e);
            }
        }
    }

    public String getBotUsername() {
        return BOT_USERNAME;
    }

    public String getBotToken() {
        return TOKEN;
    }
}
